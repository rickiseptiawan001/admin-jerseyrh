import FIREBASE from "../config/FIREBASE";
import { dispatchError, dispatchLoading, dispatchSuccess } from "../utils";

// Variable GET_LIST_LIGA
export const GET_LIST_LIGA = "GET_LIST_LIGA";

// Variable TAMBAH_LIGA
export const TAMBAH_LIGA = "TAMBAH_LIGA";

// Variable GET_DETAIL_LIGA
export const GET_DETAIL_LIGA = "GET_DETAIL_LIGA";

// Variable UPDATE_LIGA
export const UPDATE_LIGA = "UPDATE_LIGA";

// Variable DELETE_LIGA
export const DELETE_LIGA = "DELETE_LIGA";

// Function getListLiga
export const getListLiga = () => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_LIGA);

    // Firebase
    FIREBASE.database()
      // Nama table yang ingin di ambil datanya
      .ref("ligas")
      // Value
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_LIGA, data);
      })
      // Jika error
      .catch((error) => {
        // Error
        dispatchError(dispatch, GET_LIST_LIGA, error);
        alert(error);
      });
  };
};

// Function tambahLiga
export const tambahLiga = (data) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, TAMBAH_LIGA);

    // Upload ke storage firebase
    var uploadTask = FIREBASE.storage()
      // Nama folder di storage
      .ref("ligas")
      // Nama file gambar
      .child(data.imageToDB.name)
      // File yang ingin di upload di storage firebase
      .put(data.imageToDB);

    // Proses upload
    uploadTask.on(
      "state_changed",
      // Proses upload dari 0 hingga 100
      function (snapshot) {
        console.log("hasil snapshot :", snapshot);
      },

      // Jika proses terjadi error
      function (error) {
        console.log("error nya :", error);
      },

      // Jika proses upload berhasil
      function () {
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          // Object dataBaru
          const dataBaru = {
            namaLiga: data.namaLiga,
            image: downloadURL,
          };

          // Simpan ke realtime database
          FIREBASE.database()
            // Nama table
            .ref("ligas")
            // Data yang di simpan
            .push(dataBaru)
            // Jika berhasil
            .then((response) => {
              // Sukses
              dispatchSuccess(dispatch, TAMBAH_LIGA, response ? response : []);
            })
            // Jika error
            .catch((error) => {
              // Error
              dispatchError(dispatch, TAMBAH_LIGA, error);
              alert(error);
            });
        });
      }
    );
  };
};

// Function getDetailLiga
export const getDetailLiga = (id) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_DETAIL_LIGA);

    // Firebase
    FIREBASE.database()
      .ref("ligas/" + id)
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_DETAIL_LIGA, data);
      })
      .catch((error) => {
        // Error
        dispatchError(dispatch, GET_DETAIL_LIGA, error);
        alert(error);
      });
  };
};

// Function updateLiga
export const updateLiga = (data) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, UPDATE_LIGA);

    // Cek apakah gambar diganti
    if (data.imageToDB) {
      // Ambil file gambar lama dari firebase storage
      var desertRef = FIREBASE.storage().refFromURL(data.imageLama);

      // Hapus gambar lama dari firebase storage
      desertRef
        .delete()
        .then(function () {
          // Upload gambar yang baru
          var uploadTask = FIREBASE.storage()
            // Nama folder di storage
            .ref("ligas")
            // Nama file gambar
            .child(data.imageToDB.name)
            // File yang ingin di upload di storage firebase
            .put(data.imageToDB);

          uploadTask.on(
            "state_changed",
            // Proses upload dari 0 hingga 100
            function (snapshot) {
              console.log("hasil snapshot :", snapshot);
            },

            // Jika proses terjadi error
            function (error) {
              console.log("error nya :", error);
            },

            function () {
              uploadTask.snapshot.ref
                .getDownloadURL()
                .then(function (downloadURL) {
                  // Object dataBaru
                  const dataBaru = {
                    namaLiga: data.namaLiga,
                    image: downloadURL,
                  };

                  FIREBASE.database()
                    .ref("ligas/" + data.id)
                    .update(dataBaru)
                    .then((response) => {
                      // Sukses
                      dispatchSuccess(
                        dispatch,
                        UPDATE_LIGA,
                        response ? response : []
                      );
                    })
                    .catch((error) => {
                      // Error
                      dispatchError(dispatch, UPDATE_LIGA, error);
                      alert(error);
                    });
                });
            }
          );
        })
        .catch(function (error) {
          // Error
          dispatchError(dispatch, UPDATE_LIGA, error);
          alert(error);
        });
      // Jika gambar tidak diganti
    } else {
      // Object dataBaru
      const dataBaru = {
        namaLiga: data.namaLiga,
        image: data.image,
      };

      // Firebase
      FIREBASE.database()
        .ref("ligas/" + data.id)
        .update(dataBaru)
        .then((response) => {
          // Sukses
          dispatchSuccess(dispatch, UPDATE_LIGA, response ? response : []);
        })
        .catch((error) => {
          // Error
          dispatchError(dispatch, UPDATE_LIGA, error);
          alert(error);
        });
    }
  };
};

// Function deleteLiga
export const deleteLiga = (image, id) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, DELETE_LIGA);

    // Hapus gambar dari storage
    var desertRef = FIREBASE.storage().refFromURL(image);

    // Delete the file
    desertRef
      .delete()
      .then(function () {
        // Hapus juga data di realtime database
        FIREBASE.database()
          .ref("ligas/" + id)
          .remove()
          .then(() => {
            dispatchSuccess(dispatch, DELETE_LIGA, "Liga Sukses Dihapus");
          })
          .catch((error) => {
            // Error
            dispatchError(dispatch, DELETE_LIGA, error);
            alert(error);
          });
      })
      .catch(function (error) {
        // Error
        dispatchError(dispatch, DELETE_LIGA, error);
        alert(error);
      });
  };
};

