import FIREBASE from "../config/FIREBASE";
import { dispatchError, dispatchLoading, dispatchSuccess } from "../utils";
import swal from "sweetalert";

// Variable LOGIN_USER
export const LOGIN_USER = "LOGIN_USER"

// Variable CHECK_LOGIN
export const CHECK_LOGIN = "CHECK_LOGIN"

// Variable LOGOUT
export const LOGOUT_USER = "LOGOUT_USER"

// Variable GET_LIST_USER
export const GET_LIST_USER = "GET_LIST_USER"

// Function loginUser
export const loginUser = (email, password) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, LOGIN_USER)

    // Firebase cek authentication
    FIREBASE
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then((res) => {

        // Firebase cek realtime database
        FIREBASE
          .database()
          .ref(`users/${res.user.uid}`)
          .once("value")
          .then((resDB) => {
            // Jika ada datanya
            if (resDB.val()) {
              // Jika status yang login admin maka datanya simpan ke localStorage
              if (resDB.val().status === "admin") {
                window.localStorage.setItem
                  (
                    "user",
                    JSON.stringify(resDB.val())
                  )
                dispatchSuccess(dispatch, LOGIN_USER, resDB.val())
              }
              // Jika yang login bukan admin
              else {
                dispatchError(dispatch, LOGIN_USER, "Anda bukan admin, hanya admin yang boleh masuk")
                swal("Failed!", "Anda bukan admin", "error")
              }
            }
          }).catch((error) => {
            // Error
            dispatchError(dispatch, LOGIN_USER, error)
            // Sweetalert
            swal("Failed!", error, "error")
          })
      })
      .catch((error) => {
        var errorMessage = error.message
        // Error
        dispatchError(dispatch, LOGIN_USER, errorMessage)
        // Sweetalert
        swal("Failed!", "Akun tidak terdaftar", "error")
      });
  }
}

// Function checkLogin 
export const checkLogin = (history) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, CHECK_LOGIN)

    // Jika ada localStorage user
    if (window.localStorage.getItem('user')) {
      const user = JSON.parse(window.localStorage.getItem('user'))

      // Firebase
      FIREBASE
        .database()
        .ref(`users/${user.uid}`)
        .once("value")
        .then((resDB) => {
          // Jika ada datanya
          if (resDB.val()) {
            // Jika status yang login admin
            if (resDB.val().status === "admin") {
              dispatchSuccess(dispatch, CHECK_LOGIN, resDB.val())
              // Jika bukan admin, alihkan ke halaman login
            } else {
              dispatchError(dispatch, CHECK_LOGIN, "Anda bukan admin")
              history.push({ pathname: '/login' })
            }
            // Jika tidak ada datanya
          } else {
            dispatchError(dispatch, CHECK_LOGIN, "Anda bukan admin")
            history.push({ pathname: '/login' })
          }
          // Jika error
        }).catch((error) => {
          dispatchError(dispatch, CHECK_LOGIN, error)
          history.push({ pathname: '/login' })
        })
      // Jika tidak ada localStorage user
    } else {
      dispatchError(dispatch, CHECK_LOGIN, "Anda belum login")
      history.push({ pathname: '/login' })
    }
  }
}

// Function logout
export const logoutUser = (history) => {

  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, LOGOUT_USER)

    // Firebase
    FIREBASE
      .auth()
      .signOut()
      .then((res) => {
        // Sign-out successful.
        // Hapus localStorage user'
        window.localStorage.removeItem('user')
        dispatchSuccess(dispatch, LOGOUT_USER, res)
        swal("Sukses", "Logout berhasil", "success")

        // Alihkan ke halaman login jika berhasil logout
        history.push({ pathname: '/login' })
      }).catch((error) => {
        // An error happened.
        // Jika gagal logout
        dispatchError(dispatch, LOGOUT_USER, error.message)
        swal("Error!", error.message, "error")
      });
  }
}

// Function getListUser
export const getListUser = () => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_USER);

    // Firebase
    FIREBASE.database()
      .ref("users")
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_USER, data);
      })
      // Error
      .catch((error) => {
        dispatchError(dispatch, GET_LIST_USER, error);
        alert(error);
      });
  };
}