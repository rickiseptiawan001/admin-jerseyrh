import FIREBASE from "../config/FIREBASE";
import { dispatchError, dispatchLoading, dispatchSuccess } from "../utils";

// Variable GET_LIST_LIGA
export const GET_LIST_JERSEY = "GET_LIST_JERSEY";

// Variable TAMBAH_JERSEY
export const TAMBAH_JERSEY = "TAMBAH_JERSEY";

// Variable UPLOAD_JERSEY
export const UPLOAD_JERSEY = "UPLOAD_JERSEY";

// Variable GET_DETAIL_JERSEY
export const GET_DETAIL_JERSEY = "GET_DETAIL_JERSEY";

// Variable UPDATE_JERSEY
export const UPDATE_JERSEY = "UPDATE_JERSEY";

// Variable DELETE_JERSEY
export const DELETE_JERSEY = "DELETE_JERSEY";

// Function getListJersey
export const getListJersey = () => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_JERSEY);

    // Firebase
    FIREBASE.database()
      .ref("jerseys")
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_JERSEY, data);
      })
      // Error
      .catch((error) => {
        dispatchError(dispatch, GET_LIST_JERSEY, error);
        alert(error);
      });
  };
};

// Function uploadJersey
export const uploadJersey = (gambar, imageToDB) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, UPLOAD_JERSEY);

    // Upload ke storage firebase
    var uploadTask = FIREBASE.storage()
      .ref("jerseys")
      .child(gambar.name)
      .put(gambar);

    uploadTask.on(
      "state_changed",
      // Proses upload dari 0 hingga 100
      function (snapshot) {
        console.log("hasil snapshot :", snapshot);
      },

      // Jika proses terjadi error
      function (error) {
        // Error
        dispatchError(dispatch, UPLOAD_JERSEY, error);
      },

      function () {
        uploadTask.snapshot.ref.getDownloadURL().then(function (downloadURL) {
          // Object dataBaru
          const dataBaru = {
            image: downloadURL,
            imageToDB: imageToDB,
          };

          // Sukses
          dispatchSuccess(dispatch, UPLOAD_JERSEY, dataBaru);
        });
      }
    );
  };
};

// Function tambahJersey
export const tambahJersey = (data) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, TAMBAH_JERSEY);

    // Object dataBaru
    const dataBaru = {
      gambar: [data.imageToDB1, data.imageToDB2],
      nama: data.nama,
      harga: data.harga,
      berat: data.berat,
      jenis: data.jenis,
      ready: data.ready,
      ukuran: data.ukuranSelected,
      liga: data.liga,
      klub: data.klub,
      deskripsi: data.deskripsi,
    };

    // Firebase
    FIREBASE.database()
      .ref("jerseys")
      .push(dataBaru)
      .then((response) => {
        // Sukses
        dispatchSuccess(dispatch, TAMBAH_JERSEY, response);
      })
      .catch((error) => {
        // Error
        dispatchError(dispatch, TAMBAH_JERSEY, error);
        alert(error);
      });
  };
};

// Function getDetailJersey
export const getDetailJersey = (id) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_DETAIL_JERSEY);

    // Firebase
    FIREBASE.database()
      .ref("jerseys/" + id)
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_DETAIL_JERSEY, data);
      })
      .catch((error) => {
        // Error
        dispatchError(dispatch, GET_DETAIL_JERSEY, error);
        alert(error);
      });
  };
};

// Function updateJersey
export const updateJersey = (data) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, UPDATE_JERSEY);

    // Object dataBaru
    const dataBaru = {
      gambar: [
        data.imageToDB1 ? data.imageToDB1 : data.imageLama1,
        data.imageToDB2 ? data.imageToDB2 : data.imageLama2,
      ],
      nama: data.nama,
      harga: data.harga,
      berat: data.berat,
      jenis: data.jenis,
      ready: data.ready,
      ukuran: data.ukuranSelected,
      liga: data.liga,
      klub: data.klub,
      deskripsi: data.deskripsi,
    };

    // Firebase
    FIREBASE.database()
      .ref("jerseys/" + data.id)
      .update(dataBaru)
      .then((response) => {

        // Jika gambar 1 ada
        if (data.imageToDB1) {
          var desertRef = FIREBASE.storage().refFromURL(data.imageLama1);
          desertRef
            .delete()
            .catch(function (error) {
              // Error
              dispatchError(dispatch, UPDATE_JERSEY, error);
            })
        }

        // Jika gambar 2 ada
        if (data.imageToDB2) {
          var desertRef2 = FIREBASE.storage().refFromURL(data.imageLama2);
          desertRef2
            .delete()
            .catch(function (error) {
              // Error
              dispatchError(dispatch, UPDATE_JERSEY, error);
            })
        }

        // Sukses
        dispatchSuccess(dispatch, UPDATE_JERSEY, "Update Jersey Sukses");
      })
      .catch((error) => {
        // Error
        dispatchError(dispatch, UPDATE_JERSEY, error);
        alert(error);
      });
  };
};

// Function deleteJersey
export const deleteJersey = (images, id) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, DELETE_JERSEY);

    // Hapus gambar[0] dari storage
    var desertRef = FIREBASE.storage().refFromURL(images[0]);

    // Delete the file
    desertRef
      .delete()
      .then(function () {
        // Hapus gambar[1] dari storage
        var desertRef2 = FIREBASE.storage().refFromURL(images[1]);
        desertRef2
          .delete()
          .then(function () {
            // Hapus realtime database
            FIREBASE.database().ref('jerseys/' + id).remove()
              .then(function () {
                // Sukses
                dispatchSuccess(dispatch, DELETE_JERSEY, "Jersey Berhasil di Hapus")
              })
              .catch(function (error) {
                // Error
                dispatchError(dispatch, DELETE_JERSEY, error);
              })
          })
          .catch(function (error) {
            // Error
            dispatchError(dispatch, DELETE_JERSEY, error);
          })
      })
      .catch(function (error) {
        // Error
        dispatchError(dispatch, DELETE_JERSEY, error);
      })

  }
}
