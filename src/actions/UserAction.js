import FIREBASE from "../config/FIREBASE";
import { dispatchError, dispatchLoading, dispatchSuccess } from "../utils";

// Variable GET_LIST_USER
export const GET_LIST_USER = "GET_LIST_USER";

// Function getListUser
export const getListUser = () => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_USER);

    // Firebase
    FIREBASE.database()
      .ref("users")
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_USER, data);
      })
      // Error
      .catch((error) => {
        dispatchError(dispatch, GET_LIST_USER, error);
        alert(error);
      });
  };
};