import FIREBASE from "../config/FIREBASE";
import { dispatchError, dispatchLoading, dispatchSuccess } from "../utils";

// Variable GET_LIST_PESANAN
export const GET_LIST_PESANAN = "GET_LIST_PESANAN";

// Variable UPDATE_PESANAN
export const UPDATE_PESANAN = "UPDATE_PESANAN";

// Variable GET_DETAIL_PESANAN
export const GET_DETAIL_PESANAN = "GET_DETAIL_PESANAN";

// Function getListPesanan
export const getListPesanan = () => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_LIST_PESANAN);

    // Firebase
    FIREBASE.database()
      .ref("histories")
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_LIST_PESANAN, data);
      })
      // Error
      .catch((error) => {
        dispatchError(dispatch, GET_LIST_PESANAN, error);
        alert(error);
      });
  };
};

// Function updatePesanan
export const updatePesanan = (order_id, transaction_status) => {
  return (dispatch) => {
    dispatchLoading(dispatch, UPDATE_PESANAN);

    const status =
      transaction_status === "settlement" || transaction_status === "capture" ? "lunas" : transaction_status;

    FIREBASE.database()
      .ref("histories")
      .child(order_id)
      .update({
        status: status
      })
      .then((response) => {
        dispatchSuccess(dispatch, UPDATE_PESANAN, response ? response : []);
      })
      .catch((error) => {
        dispatchError(dispatch, UPDATE_PESANAN, error);
        alert(error);
      });
  };
};

// Function getDetailPesanan
export const getDetailPesanan = (id) => {
  return (dispatch) => {
    // Loading
    dispatchLoading(dispatch, GET_DETAIL_PESANAN);

    // Firebase
    FIREBASE.database()
      .ref("histories/" + id)
      .once("value", (querySnapshot) => {
        // Hasil
        let data = querySnapshot.val();

        // Sukses
        dispatchSuccess(dispatch, GET_DETAIL_PESANAN, data);
      })
      .catch((error) => {
        // Error
        dispatchError(dispatch, GET_DETAIL_PESANAN, error);
        alert(error);
      });
  };
}