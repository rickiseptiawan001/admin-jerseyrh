import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  Spinner,
} from "reactstrap";

import swal from "sweetalert";
import DefaultImage from "../../assets/img/default-image.jpg";
import { tambahJersey, uploadJersey } from "../../actions/JerseyAction";
import { getListLiga } from "../../actions/LigaAction";

class TambahJersey extends Component {
  // constructor
  constructor(props) {
    super(props);

    this.state = {
      image1: DefaultImage,
      image2: DefaultImage,
      imageToDB1: false,
      imageToDB2: false,

      nama: "",
      harga: 0,
      berat: 0,
      jenis: "",
      klub: "",
      deskripsi: "",
      ukurans: ["S", "M", "L", "XL", "XXL"],
      ukuranSelected: [],
      ready: true,
      liga: "",
    };
  }

  // componentDidMount
  componentDidMount() {
    this.props.dispatch(getListLiga());
  }

  // componentDidUpdate
  componentDidUpdate(prevProps) {
    const { uploadJerseyResult, tambahJerseyResult } = this.props;

    if (
      uploadJerseyResult &&
      prevProps.uploadJerseyResult !== uploadJerseyResult
    ) {
      this.setState({
        [uploadJerseyResult.imageToDB]: uploadJerseyResult.image,
      });

      swal("Sukses", "Gambar Berhasil di Upload", "success");
    }

    if (
      tambahJerseyResult &&
      prevProps.tambahJerseyResult !== tambahJerseyResult
    ) {
      swal("Sukses", "Jersey Sukses Dibuat", "success");
      this.props.history.push("/admin/jersey");
    }
  }

  // Function handleChange
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  // Function handleCheck
  handleCheck = (event) => {
    const checked = event.target.checked;
    const value = event.target.value;
    if (checked) {
      // Jika user ceklis ukuran
      // isi state array ukuran selected
      this.setState({
        ukuranSelected: [...this.state.ukuranSelected, value],
      });
    } else {
      // Jika user menghapus ceklis ukuran
      const ukuranBaru = this.state.ukuranSelected
        .filter((ukuran) => ukuran !== value)
        .map((filterUkuran) => {
          return filterUkuran;
        });

      this.setState({
        ukuranSelected: ukuranBaru,
      });
    }
  };

  // Function handleImage
  handleImage = (event, imageToDB) => {
    if (event.target.files && event.target.files[0]) {
      const gambar = event.target.files[0];
      this.setState({
        [event.target.name]: URL.createObjectURL(gambar),
      });

      this.props.dispatch(uploadJersey(gambar, imageToDB));
    }
  };

  // Function handleSubmit
  handleSubmit = (event) => {
    // State
    const {
      berat,
      harga,
      nama,
      liga,
      ukuranSelected,
      jenis,
      klub,
      deskripsi,
      imageToDB1,
      imageToDB2,
    } = this.state;

    // Agar page tidak reload ketika di submit
    event.preventDefault();

    // Jika semua form diisi semua
    if (
      nama &&
      liga &&
      harga &&
      berat &&
      ukuranSelected &&
      jenis &&
      klub &&
      deskripsi &&
      imageToDB1 &&
      imageToDB2
    ) {
      // tambahJersey
      this.props.dispatch(tambahJersey(this.state));
    } else {
      // Sweetalert
      swal("Failed!", "Maaf form tidak boleh kosong", "error");
    }
  };

  render() {
    // State
    const {
      berat,
      harga,
      image1,
      image2,
      imageToDB1,
      imageToDB2,
      jenis,
      liga,
      nama,
      ready,
      ukurans,
      klub,
      deskripsi,
    } = this.state;

    // Props
    const {
      getListLigaResult,
      tambahJerseyLoading
    } = this.props;

    return (
      <div className="content">
        <Row>
          <Col>
            <Card>
              <CardBody>
                <form onSubmit={(event) => this.handleSubmit(event)}>
                  <Row>
                    <Col md={6}>
                      <Row>
                        {/* Foto jersey depan[0] */}
                        <Col>
                          <img
                            src={image1}
                            width="300"
                            alt="Foto Jersey (Depan)"
                          />
                          <FormGroup>
                            <label>Foto Jersey (Depan)</label>
                            <Input
                              type="file"
                              name="image1"
                              onChange={(event) =>
                                this.handleImage(event, "imageToDB1")
                              }
                            />
                          </FormGroup>
                          {image1 !== DefaultImage ? (
                            // Jika selesai upload
                            imageToDB1 ? (
                              <p>
                                <i className="nc-icon nc-check-2"></i> Selesai
                                Upload
                              </p>
                            ) : (
                              // Jika proses upload
                              <p>
                                <i className="nc-icon nc-user-run"></i> Proses
                                Upload
                              </p>
                            )
                          ) : (
                            // Jika belum upload
                            <p>
                              <i className="nc-icon nc-cloud-upload-94"></i>{" "}
                              Belum Upload
                            </p>
                          )}
                        </Col>
                        {/* End Foto jersey depan[0] */}

                        {/* Foto jersey belakang[1] */}
                        <Col>
                          <img
                            src={image2}
                            width="300"
                            alt="Foto Jersey (Belakang)"
                          />
                          <FormGroup>
                            <label>Foto Jersey (Belakang)</label>
                            <Input
                              type="file"
                              name="image2"
                              onChange={(event) =>
                                this.handleImage(event, "imageToDB2")
                              }
                            />
                          </FormGroup>
                          {image2 !== DefaultImage ? (
                            // Selesai upload / proses upload
                            imageToDB2 ? (
                              <p>
                                <i className="nc-icon nc-check-2"></i> Selesai
                                Upload
                              </p>
                            ) : (
                              <p>
                                <i className="nc-icon nc-user-run"></i> Proses
                                Upload
                              </p>
                            )
                          ) : (
                            // Belum upload
                            <p>
                              <i className="nc-icon nc-cloud-upload-94"></i>{" "}
                              Belum Upload
                            </p>
                          )}
                        </Col>
                        {/* End Foto jersey belakang[1] */}
                      </Row>
                    </Col>

                    <Col md={6}>
                      {/* Nama Jersey & Nama Klub */}
                      <Row>
                        <Col md={6}>
                          <FormGroup>
                            <label>Nama Jersey</label>
                            <Input
                              type="text"
                              value={nama}
                              name="nama"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <label>Nama Klub</label>
                            <Input
                              type="text"
                              value={klub}
                              name="klub"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* Liga dan Harga */}
                      <Row>
                        <Col md={6}>
                          <FormGroup>
                            <label>Liga</label>
                            <Input
                              type="select"
                              name="liga"
                              value={liga}
                              onChange={(event) => this.handleChange(event)}
                            >
                              <option value="">--Pilih--</option>
                              {Object.keys(getListLigaResult).map((key) => (
                                <option value={key} key={key}>
                                  {getListLigaResult[key].namaLiga}
                                </option>
                              ))}
                            </Input>
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <label>Harga (Rp.)</label>
                            <Input
                              type="number"
                              value={harga}
                              name="harga"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* Deskripsi */}
                      <Row>
                        <Col md={12}>
                          <FormGroup>
                            <label>Deskripsi</label>
                            <Input
                              type="textarea"
                              value={deskripsi}
                              name="deskripsi"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* Jenis dan Berat */}
                      <Row>
                        <Col md={6}>
                          <FormGroup>
                            <label>Berat (kg)</label>
                            <Input
                              type="number"
                              value={berat}
                              name="berat"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <label>Jenis</label>
                            <Input
                              type="text"
                              value={jenis}
                              name="jenis"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* Ukuran & Ready */}
                      <Row>
                        <Col md={6}>
                          <label>Ukuran</label>
                          <FormGroup check>
                            {ukurans.map((ukuran, index) => (
                              <Label key={index} check className="mr-2">
                                <Input
                                  type="checkbox"
                                  value={ukuran}
                                  onChange={(event) => this.handleCheck(event)}
                                />
                                {ukuran}
                                <span className="form-check-sign">
                                  <span className="check"></span>
                                </span>
                              </Label>
                            ))}
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <label>Ready</label>
                            <Input
                              type="select"
                              name="ready"
                              value={ready}
                              onChange={(event) => this.handleChange(event)}
                            >
                              <option value={true}>Ada</option>
                              <option value={false}>Kosong</option>
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                  </Row>

                  {/* Tombol submit & loading */}
                  <Row>
                    <Col>
                      {tambahJerseyLoading ? (
                        <Button
                          type="submit"
                          color="primary"
                          className="float-right"
                          disabled
                        >
                          <Spinner size="sm" color="light" /> Loading . . .
                        </Button>
                      ) : (
                        <Button
                          type="submit"
                          color="primary"
                          className="float-right"
                        >
                          Submit
                        </Button>
                      )}
                    </Col>
                    {/* Tombol kembali */}
                    <Link to="/admin/jersey" className="btn mr-3 btn-secondary">
                      Kembali
                    </Link>
                  </Row>
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  getListLigaLoading: state.LigaReducer.getListLigaLoading,
  getListLigaResult: state.LigaReducer.getListLigaResult,
  getListLigaError: state.LigaReducer.getListLigaError,

  uploadJerseyLoading: state.JerseyReducer.uploadJerseyLoading,
  uploadJerseyResult: state.JerseyReducer.uploadJerseyResult,
  uploadJerseyError: state.JerseyReducer.uploadJerseyError,

  tambahJerseyLoading: state.JerseyReducer.tambahJerseyLoading,
  tambahJerseyResult: state.JerseyReducer.tambahJerseyResult,
  tambahJerseyError: state.JerseyReducer.tambahJerseyError,
});

export default connect(mapStateToProps, null)(TambahJersey);
