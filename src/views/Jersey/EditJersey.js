import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import DefaultImage from "../../assets/img/default-image.jpg";

import {
  Button,
  Card,
  CardBody,
  Col,
  FormGroup,
  Input,
  Label,
  Row,
  Spinner,
} from "reactstrap";

import { getListLiga } from "../../actions/LigaAction";

import {
  getDetailJersey,
  updateJersey,
  uploadJersey,
} from "../../actions/JerseyAction";

class EditJersey extends Component {
  // constructor
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      image1: DefaultImage,
      image2: DefaultImage,
      imageToDB1: false,
      imageToDB2: false,
      imageLama1: false,
      imageLama2: false,
      nama: "",
      harga: 0,
      berat: 0,
      jenis: "",
      klub: "",
      deskripsi: "",
      ukurans: ["S", "M", "L", "XL", "XXL"],
      ukuranSelected: [],
      ready: true,
      liga: "",
      editUkuran: false,
    };
  }

  // componentDidMount
  componentDidMount() {
    this.props.dispatch(getListLiga());
    this.props.dispatch(getDetailJersey(this.props.match.params.id));
  }

  // componentDidUpdate
  componentDidUpdate(prevProps) {
    // Props
    const {
      uploadJerseyResult,
      updateJerseyResult,
      getDetailJerseyResult,
    } = this.props;

    if (uploadJerseyResult && prevProps.uploadJerseyResult !== uploadJerseyResult) {
      this.setState({
        [uploadJerseyResult.imageToDB]: uploadJerseyResult.image,
      });
      // Sweetalert
      swal("Sukses", "Foto berhasil di upload", "success");
    }

    if (updateJerseyResult && prevProps.updateJerseyResult !== updateJerseyResult) {
      // Sweetalert
      swal("Sukses", updateJerseyResult, "success");
      // Alihkan kehalaman master jersey
      this.props.history.push("/admin/jersey");
    }

    if (getDetailJerseyResult && prevProps.getDetailJerseyResult !== getDetailJerseyResult) {
      this.setState({
        image1: getDetailJerseyResult.gambar[0],
        image2: getDetailJerseyResult.gambar[1],
        imageLama1: getDetailJerseyResult.gambar[0],
        imageLama2: getDetailJerseyResult.gambar[1],
        nama: getDetailJerseyResult.nama,
        harga: getDetailJerseyResult.harga,
        berat: getDetailJerseyResult.berat,
        jenis: getDetailJerseyResult.jenis,
        klub: getDetailJerseyResult.klub,
        deskripsi: getDetailJerseyResult.deskripsi,
        ukuranSelected: getDetailJerseyResult.ukuran,
        ready: getDetailJerseyResult.ready,
        liga: getDetailJerseyResult.liga,
      });
    }
  }

  // Function handleChange
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  // Function handleCheck
  handleCheck = (event) => {
    const checked = event.target.checked;
    const value = event.target.value;
    if (checked) {
      // Jika user ceklis ukuran
      // isi state array ukuran selected
      this.setState({
        ukuranSelected: [...this.state.ukuranSelected, value],
      });
    } else {
      // Jika user menghapus ceklis ukuran
      const ukuranBaru = this.state.ukuranSelected
        .filter((ukuran) => ukuran !== value)
        .map((filterUkuran) => {
          return filterUkuran;
        });

      this.setState({
        ukuranSelected: ukuranBaru,
      });
    }
  };

  // Function handleImage
  handleImage = (event, imageToDB) => {
    if (event.target.files && event.target.files[0]) {
      const gambar = event.target.files[0];
      this.setState({
        [event.target.name]: URL.createObjectURL(gambar),
      });

      this.props.dispatch(uploadJersey(gambar, imageToDB));
    }
  };

  // Function handleSubmit
  handleSubmit = (event) => {
    // State
    const {
      berat,
      harga,
      nama,
      liga,
      ukuranSelected,
      jenis,
      klub,
      deskripsi
    } = this.state;

    event.preventDefault();

    // Jika diisi semua form nya
    if (
      nama &&
      liga &&
      harga &&
      berat &&
      ukuranSelected &&
      jenis &&
      klub &&
      deskripsi
    ) {
      // updateJersey
      this.props.dispatch(updateJersey(this.state));
    } else {
      // Sweetalert
      swal("Failed", "Maaf semua form wajib diisi", "error");
    }
  };

  // Function editUkuran
  editUkuran = () => {
    this.setState({
      editUkuran: true,
      ukuranSelected: []
    });
  };

  render() {
    // State
    const {
      berat,
      harga,
      image1,
      image2,
      imageToDB1,
      imageToDB2,
      jenis,
      klub,
      deskripsi,
      liga,
      nama,
      ready,
      ukurans,
      ukuranSelected,
      editUkuran,
      imageLama1,
      imageLama2
    } = this.state;

    // Props
    const { getListLigaResult, updateJerseyLoading } = this.props;

    return (
      <div className="content">
        <Row>
          <Col>
            <Card>
              <CardBody>
                <form onSubmit={(event) => this.handleSubmit(event)}>
                  <Row>
                    <Col md={6}>
                      <Row>
                        {/* Foto jersey depan[0] */}
                        <Col>
                          <img
                            src={image1}
                            width="300"
                            alt="Foto Jersey (Depan)"
                          />
                          <FormGroup>
                            <label>Foto Jersey (Depan)</label>
                            <Input
                              type="file"
                              name="image1"
                              onChange={(event) =>
                                this.handleImage(event, "imageToDB1")
                              }
                            />
                          </FormGroup>
                          {image1 !== imageLama1 ? (
                            //selesai upload / proses upload
                            imageToDB1 ? (
                              <p>
                                <i className="nc-icon nc-check-2"></i> Selesai
                                Upload
                              </p>
                            ) : (
                              <p>
                                <i className="nc-icon nc-user-run"></i> Proses
                                Upload
                              </p>
                            )
                          ) : (
                            //belum upload
                            <p>
                              <i className="nc-icon nc-cloud-upload-94"></i>{" "}
                              Belum Upload
                            </p>
                          )}
                        </Col>
                        {/* End Foto jersey depan[0] */}

                        {/* Foto jersey belakang[1] */}
                        <Col>
                          <img
                            src={image2}
                            width="300"
                            alt="Foto Jersey (Belakang)"
                          />
                          <FormGroup>
                            <label>Foto Jersey (Belakang)</label>
                            <Input
                              type="file"
                              name="image2"
                              onChange={(event) =>
                                this.handleImage(event, "imageToDB2")
                              }
                            />
                          </FormGroup>
                          {image2 !== imageLama2 ? (
                            //selesai upload / proses upload
                            imageToDB2 ? (
                              <p>
                                <i className="nc-icon nc-check-2"></i> Selesai
                                Upload
                              </p>
                            ) : (
                              <p>
                                <i className="nc-icon nc-user-run"></i> Proses
                                Upload
                              </p>
                            )
                          ) : (
                            //belum upload
                            <p>
                              <i className="nc-icon nc-cloud-upload-94"></i>{" "}
                              Belum Upload
                            </p>
                          )}
                        </Col>
                        {/* End Foto jersey belakang[1] */}
                      </Row>
                    </Col>
                    <Col md={6}>
                      {/* Nama Jersey & Nama Klub */}
                      <Row>
                        <Col md={6}>
                          <FormGroup>
                            <label>Nama Jersey</label>
                            <Input
                              type="text"
                              value={nama}
                              name="nama"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <label>Nama Klub</label>
                            <Input
                              type="text"
                              value={klub}
                              name="klub"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      <Row>
                        <Col md={6}>
                          {/* Liga */}
                          <FormGroup>
                            <label>Liga</label>
                            <Input
                              type="select"
                              name="liga"
                              value={liga}
                              onChange={(event) => this.handleChange(event)}
                            >
                              <option value="">--Pilih--</option>
                              {Object.keys(getListLigaResult).map((key) => (
                                <option value={key} key={key}>
                                  {getListLigaResult[key].namaLiga}
                                </option>
                              ))}
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col md={6}>
                          {/* Harga */}
                          <FormGroup>
                            <label>Harga (Rp.)</label>
                            <Input
                              type="number"
                              value={harga}
                              name="harga"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* Deskripsi */}
                      <Row>
                        <Col md={12}>
                          <FormGroup>
                            <label>Deskripsi</label>
                            <Input
                              type="textarea"
                              value={deskripsi}
                              name="deskripsi"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* Berat dan Jenis */}
                      <Row>
                        <Col md={6}>
                          <FormGroup>
                            <label>Berat (kg)</label>
                            <Input
                              type="number"
                              value={berat}
                              name="berat"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <label>Jenis</label>
                            <Input
                              type="text"
                              value={jenis}
                              name="jenis"
                              onChange={(event) => this.handleChange(event)}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* Ukuran dan Ready */}
                      <Row>
                        <Col md={6}>
                          <label>
                            Ukuran yang tersedia sekarang :
                            {ukuranSelected.map((item, index) => (
                              <strong key={index}> {item} </strong>
                            ))}
                          </label>
                          {editUkuran ? (
                            <>
                              {/* Checkbox ukuran */}
                              <FormGroup check>
                                {ukurans.map((ukuran, index) => (
                                  <Label key={index} check className="mr-2">
                                    <Input
                                      type="checkbox"
                                      value={ukuran}
                                      onChange={(event) =>
                                        this.handleCheck(event)
                                      }
                                    />
                                    {ukuran}
                                    <span className="form-check-sign">
                                      <span className="check"></span>
                                    </span>
                                  </Label>
                                ))}
                              </FormGroup>

                              {/* Tombol selesai edit ukuran */}
                              <Button
                                color="primary"
                                size="sm"
                                onClick={() =>
                                  this.setState({ editUkuran: false })
                                }
                              >
                                Selesai Edit Ukuran
                              </Button>
                            </>
                          ) : (
                            // Tombol edit ukuran
                            <Button
                              color="primary"
                              size="sm"
                              onClick={() => this.editUkuran()}
                            >
                              Edit Ukuran
                            </Button>
                          )}
                        </Col>

                        <Col md={6}>
                          <FormGroup>
                            <label>Ready</label>
                            <Input
                              type="select"
                              name="ready"
                              value={ready}
                              onChange={(event) => this.handleChange(event)}
                            >
                              <option value={true}>Ada</option>
                              <option value={false}>Kosong</option>
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                  </Row>

                  <Row>
                    <Col>
                      {/* Jika loading */}
                      {updateJerseyLoading ? (
                        <Button
                          type="submit"
                          color="primary"
                          className="float-right"
                          disabled
                        >
                          <Spinner size="sm" color="light" /> Loading . . .
                        </Button>
                      ) : (
                        // Button submit
                        <Button
                          type="submit"
                          color="primary"
                          className="float-right"
                        >
                          Submit
                        </Button>
                      )}
                    </Col>

                    {/* Tombol kembali */}
                    <Link to="/admin/jersey" className="btn mr-3 btn-secondary">
                      Kembali
                    </Link>
                  </Row>
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  getListLigaLoading: state.LigaReducer.getListLigaLoading,
  getListLigaResult: state.LigaReducer.getListLigaResult,
  getListLigaError: state.LigaReducer.getListLigaError,

  uploadJerseyLoading: state.JerseyReducer.uploadJerseyLoading,
  uploadJerseyResult: state.JerseyReducer.uploadJerseyResult,
  uploadJerseyError: state.JerseyReducer.uploadJerseyError,

  getDetailJerseyLoading: state.JerseyReducer.getDetailJerseyLoading,
  getDetailJerseyResult: state.JerseyReducer.getDetailJerseyResult,
  getDetailJerseyError: state.JerseyReducer.getDetailJerseyError,

  updateJerseyLoading: state.JerseyReducer.updateJerseyLoading,
  updateJerseyResult: state.JerseyReducer.updateJerseyResult,
  updateJerseyError: state.JerseyReducer.updateJerseyError,
});

export default connect(mapStateToProps, null)(EditJersey);
