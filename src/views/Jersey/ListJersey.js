import React, { Component } from "react";
import { connect } from "react-redux";
import swal from "sweetalert";

import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Spinner,
  Button,
} from "reactstrap";

import { Link } from "react-router-dom";
import { rupiah } from "../../utils";

import {
  deleteJersey,
  getListJersey
} from "../../actions/JerseyAction";


// Datatable bootstrap 4
import "datatables.net-bs4/css/dataTables.bootstrap4.min.css";
import "datatables.net-bs4/js/dataTables.bootstrap4.min.js";

// Jquery
import "jquery/dist/jquery.min.js";
import $ from "jquery";

class ListJersey extends Component {
  // componentDidMount
  componentDidMount() {
    // getListJersey
    this.props.dispatch(getListJersey());

    // Initialize datatable
    $(document).ready(function () {
      setTimeout(function () {
        $("#myTable").DataTable({
          "pageLength": 100 - 98,
          "lengthMenu": [2, 10, 50, 100]
        });
      }, 1000);
    });
  }

  // componentDidUpdate
  componentDidUpdate(prevProps) {
    // Props
    const { deleteJerseyResult } = this.props;

    if (deleteJerseyResult && prevProps.deleteJerseyResult !== deleteJerseyResult) {
      swal("Sukses!", deleteJerseyResult, "success");
      this.props.dispatch(getListJersey());
    }
  }

  // Function removeData
  removeData = (images, key) => {
    // deleteJersey
    this.props.dispatch(deleteJersey(images, key));
  }

  render() {
    // Props
    const {
      getListJerseyError,
      getListJerseyLoading,
      getListJerseyResult
    } = this.props;

    return (
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                {/* Tombol tambah jersey */}
                <Link
                  to="/admin/jersey/tambah"
                  className="btn btn-primary float-right"
                >
                  Tambah Jersey
                </Link>
              </CardHeader>
              <CardBody>
                <Table id="myTable">
                  <thead className="text-primary">
                    <tr>
                      <th>Foto</th>
                      <th>Nama Jersey</th>
                      <th>Harga</th>
                      <th>Berat</th>
                      <th>Jenis</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>

                  <tbody>
                    {/* Jika ada datanya */}
                    {getListJerseyResult ? (
                      Object.keys(getListJerseyResult).map((key) => (
                        <tr key={key}>
                          {/* Foto */}
                          <td>
                            <img
                              src={getListJerseyResult[key].gambar[0]}
                              width="100"
                              alt={getListJerseyResult[key].nama}
                            />
                          </td>

                          {/* Nama */}
                          <td>{getListJerseyResult[key].nama}</td>

                          {/* Harga */}
                          <td>Rp. {rupiah(getListJerseyResult[key].harga)}</td>

                          {/* Berat */}
                          <td>{getListJerseyResult[key].berat} kg</td>

                          {/* Jenis */}
                          <td>{getListJerseyResult[key].jenis} </td>

                          {/* Tombol */}
                          <td>
                            {/* Tombol edit */}
                            <Link
                              className="btn btn-warning"
                              to={"/admin/jersey/edit/" + key}
                            >
                              Edit
                            </Link>

                            {/* Tombol hapus */}
                            <Button
                              color="danger"
                              className="ml-2"
                              onClick={() => this.removeData(getListJerseyResult[key].gambar, key)}
                            >
                              Hapus
                            </Button>
                          </td>
                        </tr>
                      ))
                      // Jika loading
                    ) : getListJerseyLoading ? (
                      <tr>
                        <td colSpan="6" align="center">
                          <Spinner color="primary" />
                        </td>
                      </tr>
                      // Jika error
                    ) : getListJerseyError ? (
                      <tr>
                        <td colSpan="6" align="center">
                          {getListJerseyError}
                        </td>
                      </tr>
                      // Jika data kosong
                    ) : (
                      <tr>
                        <td colSpan="6" align="center">
                          Data Kosong
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  getListJerseyLoading: state.JerseyReducer.getListJerseyLoading,
  getListJerseyResult: state.JerseyReducer.getListJerseyResult,
  getListJerseyError: state.JerseyReducer.getListJerseyError,

  deleteJerseyLoading: state.JerseyReducer.deleteJerseyLoading,
  deleteJerseyResult: state.JerseyReducer.deleteJerseyResult,
  deleteJerseyError: state.JerseyReducer.deleteJerseyError,
});

export default connect(mapStateToProps, null)(ListJersey);
