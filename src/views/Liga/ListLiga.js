import {
  getListLiga,
  deleteLiga
} from "actions/LigaAction";

import React, { Component } from "react";
import { connect } from "react-redux";
import swal from "sweetalert";
import { Link } from "react-router-dom";

import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Button,
  Spinner,
} from "reactstrap";

// Datatable bootstrap 4
import "datatables.net-bs4/css/dataTables.bootstrap4.min.css";
import "datatables.net-bs4/js/dataTables.bootstrap4.min.js";

// Jquery
import "jquery/dist/jquery.min.js";
import $ from "jquery";

class ListLiga extends Component {
  // componentDidMount
  componentDidMount() {
    // getListLiga
    this.props.dispatch(getListLiga());

    // Initialize datatable
    $(document).ready(function () {
      setTimeout(function () {
        $("#myTable").DataTable({
          "pageLength": 2,
          "lengthMenu": [2, 10, 50, 100]
        });
      }, 1000);
    });
  }

  // componentDidUpdate
  componentDidUpdate(prevProps) {
    // Props
    const { deleteLigaResult } = this.props;

    if (deleteLigaResult && prevProps.deleteLigaResult !== deleteLigaResult) {
      // Sweetalert
      swal("Sukses!", deleteLigaResult, "success");
      // getListLiga
      this.props.dispatch(getListLiga());
    }
  }

  // Function removeData
  removeData = (image, id) => {
    // deleteLiga
    this.props.dispatch(deleteLiga(image, id))
  };

  render() {
    // Props
    const {
      getListLigaResult,
      getListLigaLoading,
      getListLigaError
    } = this.props;

    return (
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardHeader>
                {/* Tombol tambah liga */}
                <Link
                  to="/admin/liga/tambah"
                  className="btn btn-primary float-right"
                >
                  Tambah Liga
                </Link>
              </CardHeader>

              <CardBody>
                <Table id="myTable">
                  <thead className="text-primary">
                    <tr>
                      <th>Logo</th>
                      <th>Nama Liga</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>

                  <tbody>
                    {/* Jika datanya ada */}
                    {getListLigaResult ? (
                      Object.keys(getListLigaResult).map((key) => (
                        <tr key={key}>
                          <td>
                            <img
                              src={getListLigaResult[key].image}
                              width="100"
                              alt={getListLigaResult[key].namaLiga}
                            />
                          </td>
                          <td>{getListLigaResult[key].namaLiga}</td>
                          <td>
                            <Link
                              className="btn btn-warning"
                              to={"/admin/liga/edit/" + key}
                            >
                              Edit
                            </Link>

                            <Button
                              color="danger"
                              className="ml-2"
                              onClick={() => this.removeData(getListLigaResult[key].image, key)}
                            >
                              Hapus
                            </Button>
                          </td>
                        </tr>
                      ))
                      // Jika loading
                    ) : getListLigaLoading ? (
                      <tr>
                        <td colSpan="3" align="center">
                          <Spinner color="primary" />
                        </td>
                      </tr>
                      // Jika error
                    ) : getListLigaError ? (
                      <tr>
                        <td colSpan="3" align="center">
                          {getListLigaError}
                        </td>
                      </tr>
                      // Jika data kosong
                    ) : (
                      <tr>
                        <td colSpan="3" align="center">
                          Data Kosong
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  getListLigaResult: state.LigaReducer.getListLigaResult,
  getListLigaLoading: state.LigaReducer.getListLigaLoading,
  getListLigaError: state.LigaReducer.getListLigaError,

  deleteLigaLoading: state.LigaReducer.deleteLigaLoading,
  deleteLigaResult: state.LigaReducer.deleteLigaResult,
  deleteLigaError: state.LigaReducer.deleteLigaError,
});

export default connect(mapStateToProps, null)(ListLiga);
