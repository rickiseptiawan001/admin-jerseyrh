import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import swal from "sweetalert";
import DefaultImage from "../../assets/img/default-image.jpg";

import {
  Row,
  Col,
  Card,
  CardBody,
  FormGroup,
  Input,
  Button,
  Spinner,
} from "reactstrap";

import {
  updateLiga,
  getDetailLiga
} from "actions/LigaAction";

class EditLiga extends Component {
  // constructor
  constructor(props) {
    super(props);

    this.state = {
      id: this.props.match.params.id,
      imageLama: DefaultImage,
      image: DefaultImage,
      imageToDB: false,
      namaLiga: "",
    };
  }

  // componentDidMount
  componentDidMount() {
    // this.props.match.params.id = id liga
    this.props.dispatch(getDetailLiga(this.props.match.params.id));
  }

  // Function handleChange
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  // Function handleImage
  handleImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const gambar = event.target.files[0];
      this.setState({
        image: URL.createObjectURL(gambar),
        imageToDB: gambar,
      });
    }
  };

  // Function handleSubmit
  handleSubmit = (event) => {
    // State
    const { namaLiga } = this.state;
    event.preventDefault();

    // Jika nama liga di isi
    if (namaLiga) {
      // updateLiga
      this.props.dispatch(updateLiga(this.state));
    } else {
      // Sweetalert
      swal("Failed!", "Maaf Nama Liga harus diisi", "error");
    }
  };

  // componentDidUpdate
  componentDidUpdate(prevProps) {
    // Props
    const { updateLigaResult, getDetailLigaResult } = this.props;

    if (updateLigaResult && prevProps.updateLigaResult !== updateLigaResult) {
      swal("Sukses", "Liga Sukses Diupdate", "success");
      this.props.history.push("/admin/liga");
    }

    if (getDetailLigaResult && prevProps.getDetailLigaResult !== getDetailLigaResult) {
      this.setState({
        image: getDetailLigaResult.image,
        namaLiga: getDetailLigaResult.namaLiga,
        imageLama: getDetailLigaResult.image,
      })
    }
  }

  render() {
    // State
    const { image, namaLiga } = this.state;

    // Props
    const { updateLigaLoading } = this.props;
    return (
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <Row>
                  <Col md={3}>
                    {/* Foto liga */}
                    <img src={image} width="200" alt="Logo Liga" />
                  </Col>

                  <Col md={4}>
                    {/* Nama liga */}
                    <FormGroup>
                      <label>Nama Liga</label>
                      <Input
                        type="text"
                        value={namaLiga}
                        name="namaLiga"
                        onChange={(event) => this.handleChange(event)}
                      />
                    </FormGroup>
                  </Col>
                </Row>
                <form onSubmit={(event) => this.handleSubmit(event)}>
                  <Row>
                    <Col md={6}>
                      {/* Logo liga */}
                      <FormGroup>
                        <label>Logo Liga</label>
                        <Input
                          type="file"
                          onChange={(event) => this.handleImage(event)}
                        />
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    <Col>
                      {/* Jika loading */}
                      {updateLigaLoading ? (
                        <Button color="primary" type="submit" disabled>
                          <Spinner size="sm" color="light" /> Loading
                        </Button>
                      ) : (
                        // Tombol submit
                        <Button color="primary" type="submit">
                          Submit
                        </Button>
                      )}

                      {/* Tombol kembali */}
                      <Link to="/admin/liga" className="btn btn-secondary ml-2">
                        Kembali
                      </Link>
                    </Col>
                  </Row>
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  updateLigaLoading: state.LigaReducer.updateLigaLoading,
  updateLigaResult: state.LigaReducer.updateLigaResult,
  updateLigaError: state.LigaReducer.updateLigaError,

  getDetailLigaLoading: state.LigaReducer.getDetailLigaLoading,
  getDetailLigaResult: state.LigaReducer.getDetailLigaResult,
  getDetailLigaError: state.LigaReducer.getDetailLigaError,
});

export default connect(mapStateToProps, null)(EditLiga);
