import { tambahLiga } from "actions/LigaAction";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import {
  Row,
  Col,
  Card,
  CardBody,
  FormGroup,
  Input,
  Button,
  Spinner,
} from "reactstrap";

import swal from "sweetalert";
import DefaultImage from "../../assets/img/default-image.jpg";

class TambahLiga extends Component {
  // constructor
  constructor(props) {
    super(props);

    this.state = {
      image: DefaultImage,
      imageToDB: false,
      namaLiga: "",
    };
  }

  // Function handleChange
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  // Function handleImage
  handleImage = (event) => {
    if (event.target.files && event.target.files[0]) {
      const gambar = event.target.files[0];
      this.setState({
        image: URL.createObjectURL(gambar),
        imageToDB: gambar,
      });
    }
  };

  // Function handleSubmit
  handleSubmit = (event) => {
    // State
    const { imageToDB, namaLiga } = this.state;

    // Agar page tidak reload ketika di submit
    event.preventDefault();

    // Jika imageToDB dan namaLiga nya ada
    if (imageToDB && namaLiga) {
      // Proses lanjut ke action firebase
      this.props.dispatch(tambahLiga(this.state));
    } else {
      // Sweetalert
      swal("Failed!", "Maaf form tidak boleh kosong", "error");
    }
  };

  // componentDidUpdate
  componentDidUpdate(prevProps) {
    // Props
    const { tambahLigaResult } = this.props;

    if (tambahLigaResult && prevProps.tambahLigaResult !== tambahLigaResult) {
      swal("Sukses", "Liga Sukses Dibuat", "success");
      this.props.history.push("/admin/liga");
    }
  }

  render() {
    // State
    const {
      image,
      namaLiga
    } = this.state;

    // Props
    const {
      tambahLigaLoading
    } = this.props;

    return (
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <Row>
                  <Col md={3}>
                    {/* Image */}
                    <img src={image} width="200" alt="Logo Liga" />
                  </Col>

                  <Col md={4}>
                    {/* Nama Liga */}
                    <FormGroup>
                      <label>Nama Liga</label>
                      <Input
                        type="text"
                        value={namaLiga}
                        name="namaLiga"
                        onChange={(event) => this.handleChange(event)}
                      />
                    </FormGroup>
                  </Col>
                </Row>

                {/* Form */}
                <form onSubmit={(event) => this.handleSubmit(event)}>
                  <Row>
                    {/* Logo liga */}
                    <Col md={6} className="mt-2">
                      <FormGroup>
                        <label>Logo Liga</label>
                        <Input
                          type="file"
                          onChange={(event) => this.handleImage(event)}
                        />
                      </FormGroup>
                    </Col>
                  </Row>

                  <Row>
                    {/* Tombol Submit & Kembali */}
                    <Col>
                      {/* Jika loading */}
                      {tambahLigaLoading ? (
                        <Button color="primary" type="submit" disabled>
                          <Spinner size="sm" color="light" /> Loading
                        </Button>
                      ) : (
                        // Tombol submit
                        <Button color="primary" type="submit">
                          Submit
                        </Button>
                      )}

                      {/* Tombol kembali */}
                      <Link to="/admin/liga" className="btn btn-secondary ml-2">
                        Kembali
                      </Link>
                    </Col>
                  </Row>
                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  tambahLigaLoading: state.LigaReducer.tambahLigaLoading,
  tambahLigaResult: state.LigaReducer.tambahLigaResult,
  tambahLigaError: state.LigaReducer.tambahLigaError,
});

export default connect(mapStateToProps, null)(TambahLiga);
