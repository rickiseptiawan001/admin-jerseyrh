import Dashboard from "./Dashboard";

import ListLiga from "./Liga/ListLiga";
import TambahLiga from "./Liga/TambahLiga";
import EditLiga from "./Liga/EditLiga";

import ListJersey from "./Jersey/ListJersey";
import TambahJersey from "./Jersey/TambahJersey";
import EditJersey from "./Jersey/EditJersey";

import ListPesanan from "./Pesanan/ListPesanan";
import DetailPesanan from "./Pesanan/DetailPesanan";

import Finish from "./Midtrans/Finish";
import UnFinish from "./Midtrans/UnFinish";
import Gagal from "./Midtrans/Gagal";

import Login from "./Login";

export {
  Dashboard,
  ListLiga,
  ListJersey,
  TambahLiga,
  TambahJersey,
  EditJersey,
  EditLiga,
  Login,
  ListPesanan,
  DetailPesanan,
  Finish,
  UnFinish,
  Gagal,
};
