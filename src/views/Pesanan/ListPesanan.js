import React, { Component } from 'react';
import {
  Row,
  Col,
  Card,
  CardBody,
  Table,
  Spinner,
} from "reactstrap";

import { getListPesanan } from 'actions/PesananAction';
import { getListUser } from 'actions/AuthAction'
import { connect } from 'react-redux';
import { rupiah } from '../../utils';
import { PesananDetail } from '../../components';

// Datatable bootstrap 4
import "datatables.net-bs4/css/dataTables.bootstrap4.min.css";
import "datatables.net-bs4/js/dataTables.bootstrap4.min.js";

// Jquery
import "jquery/dist/jquery.min.js";
import $ from "jquery";

import { Link } from "react-router-dom";

class ListPesanan extends Component {
  // componentDidMount
  componentDidMount() {
    // getListLiga
    this.props.dispatch(getListPesanan());

    // getListUser berdasarkan id
    this.props.dispatch(getListUser());

    // Initialize datatable
    $(document).ready(function () {
      setTimeout(function () {
        $("#myTable").DataTable({
          "pageLength": 2,
          "lengthMenu": [2, 10, 50, 100]
        });
      }, 1000);
    });
  }

  render() {
    // Props
    const {
      getListPesananResult,
      getListPesananLoading,
      getListPesananError,

      getListUserResult,
    } = this.props;

    return (
      <div className="content">
        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <Table id="myTable">
                  <thead className="text-primary">
                    <tr>
                      <th>Tanggal</th>
                      <th>Pesanan</th>
                      <th>Status</th>
                      <th>Total Harga</th>
                      <th>Nama User</th>
                      <th>Action</th>
                      {/* <th>Aksi</th> */}
                    </tr>
                  </thead>

                  <tbody>
                    {/* Jika datanya ada */}
                    {getListPesananResult ? (
                      Object.keys(getListPesananResult).map((key) => (
                        <tr key={key}>
                          <td>{getListPesananResult[key].tanggal}</td>
                          <td>
                            <PesananDetail {...this.state} pesanans={getListPesananResult[key].pesanans} />
                          </td>
                          <td>{getListPesananResult[key].status}</td>
                          <td align="left">
                            <p>Total : Rp {rupiah(getListPesananResult[key].totalHarga)}</p>
                            <p>Ongkir : Rp {rupiah(getListPesananResult[key].ongkir)}</p>
                            <p>
                              Total Harga : Rp
                              &nbsp;
                              <strong>
                                {rupiah(getListPesananResult[key].totalHarga +
                                  getListPesananResult[key].ongkir)}
                              </strong>
                            </p>
                          </td>
                          <td>
                            {Object.keys(getListUserResult).map((keys) => (
                              <p>
                                {getListUserResult[keys].uid === getListPesananResult[key].user ? getListUserResult[keys].nama : ""}
                              </p>
                            ))}
                          </td>
                          <td>
                            {getListPesananResult[key].status === 'lunas' ? (
                              <Link to={"/admin/pesanan/" + key} className="btn btn-sm btn-danger">
                                Save
                              </Link>
                            ) : (
                              <span className="btn btn-sm btn-secondary">
                                Save
                              </span>
                            )}
                          </td>
                        </tr>
                      ))
                      // Jika loading
                    ) : getListPesananLoading ? (
                      <tr>
                        <td colSpan="3" align="center">
                          <Spinner color="primary" />
                        </td>
                      </tr>
                      // Jika error
                    ) : getListPesananError ? (
                      <tr>
                        <td colSpan="3" align="center">
                          {getListPesananError}
                        </td>
                      </tr>
                      // Jika data kosong
                    ) : (
                      <tr>
                        <td colSpan="3" align="center">
                          Data Kosong
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    )
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  getListPesananResult: state.PesananReducer.getListPesananResult,
  getListPesananLoading: state.PesananReducer.getListPesananLoading,
  getListPesananError: state.PesananReducer.getListPesananError,

  getListUserResult: state.AuthReducer.getListUserResult,
  getListUserLoading: state.AuthReducer.getListUserLoading,
  getListUserError: state.AuthReducer.getListUserError,
});

export default connect(mapStateToProps, null)(ListPesanan);
