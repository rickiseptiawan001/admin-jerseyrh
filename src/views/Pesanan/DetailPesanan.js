import React, { Component } from 'react';
import { rupiah } from '../../utils'
import {
  Table,
  Card,
  Row,
  Col,
  Button
} from "reactstrap";
import { getDetailPesanan } from 'actions/PesananAction';
import { getListUser } from 'actions/AuthAction'
import { connect } from 'react-redux';
import { DetailPesananComponent } from 'components';

import { PDFExport } from '@progress/kendo-react-pdf';
import Logo from '../../assets/img/Logo.svg'

class DetailPesanan extends Component {
  constructor(props) {
    super(props);
    this.pdfExportComponent = React.createRef(null);
  }
  // componentDidMount
  componentDidMount() {
    this.props.dispatch(getDetailPesanan(this.props.match.params.id));
    this.props.dispatch(getListUser());
  }

  handleExportWithComponent = () => {
    this.pdfExportComponent.current.save();
  }

  render() {
    const { getDetailPesananResult, getListUserResult } = this.props;
    return (
      <div className="content">
        <PDFExport ref={this.pdfExportComponent} paperSize="A4">
          <Card className="p-4">
            <div className="mb-2 text-center">
              <img src={Logo} width={60} alt="logo-jersey-rh" />
              <h4 className="text-primary"><b>Toko Jersey RH</b></h4>
            </div>
            <Table bordered>
              <tr>
                <th>Tanggal Pembelian</th>
                <td>{getDetailPesananResult.tanggal}</td>
              </tr>
              <tr>
                <th>Nama Pembeli</th>
                <td>
                  {Object.keys(getListUserResult).map((key) => (
                    <p key={key}>
                      {getListUserResult[key].uid === getDetailPesananResult.user ? getListUserResult[key].nama : ""}
                    </p>
                  ))}
                </td>
              </tr>
              <tr>
                <th>Order ID</th>
                <td>{getDetailPesananResult.order_id}</td>
              </tr>
              <tr>
                <th>Produk yang di beli</th>
                <td>
                  <DetailPesananComponent {...this.state} pesanans={getDetailPesananResult.pesanans} />
                </td>
              </tr>
              <tr>
                <th>Total Harga</th>
                <td>Rp {rupiah(parseInt(getDetailPesananResult.totalHarga))}</td>
              </tr>
              <tr>
                <th>Ongkir</th>
                <td>Rp {rupiah(parseInt(getDetailPesananResult.ongkir))}</td>
              </tr>
              <tr>
                <th>Total Harga + Ongkir</th>
                <td>Rp {rupiah(parseInt(getDetailPesananResult.totalHarga + getDetailPesananResult.ongkir))}</td>
              </tr>
              <tr>
                <th>Status Transaksi</th>
                <td>
                  <span className="btn btn-sm btn-success">
                    {getDetailPesananResult.status}
                  </span>
                </td>
              </tr>
            </Table>
          </Card>
        </PDFExport>
        <Row>
          <Col md={4}>
            <Button onClick={() => this.handleExportWithComponent()} className="btn btn-danger">
              Simpan Laporan
            </Button>
          </Col>
        </Row>
      </div>
    )
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  getDetailPesananResult: state.PesananReducer.getDetailPesananResult,
  getListUserResult: state.AuthReducer.getListUserResult,
});

export default connect(mapStateToProps, null)(DetailPesanan)