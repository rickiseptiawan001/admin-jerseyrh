import React, { Component } from "react";
import {
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Button,
  Spinner,
} from "reactstrap";
import FinishPict from "../../assets/img/finish.svg";
import { updatePesanan } from "../../actions/PesananAction";
import { connect } from "react-redux";

class Finish extends Component {
  constructor(props) {
    super(props);

    this.state = {
      order_id: "",
      transaction_status: "",
    };
  }

  componentDidMount() {
    let search = window.location.search;
    let params = new URLSearchParams(search);

    const order_id = params.get("order_id");
    const transaction_status = params.get("transaction_status");

    if (order_id) {
      this.setState({
        order_id: order_id,
        transaction_status: transaction_status,
      });

      // updatePesanan
      this.props.dispatch(updatePesanan(order_id, transaction_status));
    }
    // otomatis jalankan fungsi toHistory
    setTimeout(() => {
      this.toHistory()
    }, 5000);
  }

  toHistory = () => {
    window.ReactNativeWebView.postMessage("Selesai");
  };

  render() {
    const { order_id, transaction_status } = this.state;
    const { updatePesananLoading } = this.props;
    return (
      <div>
        <Row className="justify-content-center">
          {updatePesananLoading ? (
            <Spinner color="" />
          ) : (
            <Col md="4" className="mt-5">
              <Card className="p-5">
                <img src={FinishPict} alt="finish" />
                <CardHeader tag="h4" className="text-success text-center">
                  <b>SUKSES !</b>
                </CardHeader>
                <CardBody>
                  <p>
                    {transaction_status === "pending" &&
                      "Selamat Transaksi Berhasil."}
                  </p>

                  <p>Order ID : {order_id}</p>
                  <p>
                    Status Transaksi :{" "}
                    <b>
                      {transaction_status === "settlement" ||
                        transaction_status === "capture"
                        ? "lunas"
                        : transaction_status}
                    </b>
                  </p>

                  <div className="text-center mt-5">
                    <Button
                      color="primary"
                      type="submit"
                      onClick={() => this.toHistory()}
                    >
                      Lanjutkan
                    </Button>
                  </div>
                </CardBody>
              </Card>
            </Col>
          )}
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  updatePesananLoading: state.PesananReducer.updatePesananLoading,
});

export default connect(mapStateToProps, null)(Finish);
