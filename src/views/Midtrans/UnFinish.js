import React, { Component } from "react";
import { Row, Col, Card, CardHeader, CardBody, Button } from "reactstrap";
import ErrorPict from "../../assets/img/error.svg";

export default class UnFinish extends Component {
  render() {
    let search = window.location.search;
    let params = new URLSearchParams(search);

    const order_id = params.get("order_id");
    const transaction_status = params.get("transaction_status");

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="4" className="mt-5">
            <Card className="p-5">
              <img src={ErrorPict} alt="pending" />
              <CardHeader tag="h4" className="text-warning text-center ">
                <b>PENDING !</b>
              </CardHeader>
              <CardBody>
                <p>
                  Maaf transaksi anda belum selesai, silahkan klik tombol dibawah ini.
                </p>

                <p>Order ID : {order_id}</p>
                <p>
                  Status Transaksi : <b>{transaction_status}</b>
                </p>

                <div className="text-center mt-5">
                  <Button color="primary" type="submit">
                    Lanjutkan
                  </Button>
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
