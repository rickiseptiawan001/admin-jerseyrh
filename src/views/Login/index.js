import React, { Component } from 'react';
import { Row, Col, Card, CardHeader, CardBody, FormGroup, Input, Button, Spinner } from 'reactstrap';
import Logo from '../../assets/img/logo.png'
import swal from "sweetalert";
import { connect } from 'react-redux';
import { checkLogin, loginUser } from '../../actions/AuthAction';

class Login extends Component {
  // constructor
  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: ''
    }
  }

  // Function handleChange
  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  // Function handleSubmit
  handleSubmit = (event) => {
    // State
    const { email, password } = this.state
    // Validasi
    event.preventDefault()

    // Jika email & password nya sudah diisi
    if (email && password) {
      // loginUser
      this.props.dispatch(loginUser(email, password))
    } else {
      swal("Failed!", "Maaf form tidak boleh kosong", "error");
    }
  }

  // componentDidMount
  componentDidMount() {
    this.props.dispatch(checkLogin(this.props.history))
  }

  // componentDidUpdate
  componentDidUpdate(prevProps) {
    // Props
    const { loginResult, checkLoginResult } = this.props;

    if (loginResult && prevProps.loginResult !== loginResult) {
      swal("Sukses", "Login berhasil", "success");
      this.props.history.push("/admin/dashboard");
    }

    if (checkLoginResult && prevProps.checkLoginResult !== checkLoginResult) {
      swal("Informasi", "Anda sudah login sebelumnya", "warning");
      this.props.history.push("/admin/dashboard");
    }
  }

  render() {
    const { email, password } = this.state
    const { loginLoading } = this.props
    return (
      <div>
        <Row className="justify-content-center">
          <Col md="4" className="mt-5">
            <img src={Logo} className="rounded mx-auto d-block" alt="logo" />
            <Card>
              <CardHeader tag="h4">Login</CardHeader>
              <CardBody>
                <form onSubmit={(event) => this.handleSubmit(event)}>
                  {/* Email address */}
                  <FormGroup>
                    <label for="email">Email Address</label>
                    <Input
                      type="email"
                      name="email"
                      value={email}
                      placeholder="Masukan email address"
                      onChange={(event) => this.handleChange(event)}
                    />
                  </FormGroup>

                  {/* Password */}
                  <FormGroup>
                    <label for="password">Password</label>
                    <Input
                      type="password"
                      name="password"
                      value={password}
                      placeholder="Masukan password"
                      onChange={(event) => this.handleChange(event)}
                    />
                  </FormGroup>

                  {/* Tombol loading */}
                  {loginLoading ? (
                    <Button color="primary" type="submit" disabled>
                      <Spinner size="sm" color="light" /> Loading
                    </Button>
                  ) : (
                    // Tombol submit
                    <Button color="primary" type="submit">
                      Login
                    </Button>
                  )}

                </form>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div >
    )
  }
}

// mapStateToProps
const mapStateToProps = (state) => ({
  loginLoading: state.AuthReducer.loginLoading,
  loginResult: state.AuthReducer.loginResult,
  loginError: state.AuthReducer.loginError,

  checkLoginLoading: state.AuthReducer.checkLoginLoading,
  checkLoginResult: state.AuthReducer.checkLoginResult,
  checkLoginError: state.AuthReducer.checkLoginError
})

export default connect(mapStateToProps, null)(Login)
