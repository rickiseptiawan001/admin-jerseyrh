import { getListLiga } from "../actions/LigaAction";

import { getListJersey } from "../actions/JerseyAction";

import { getListPesanan } from "../actions/PesananAction";

import { getListUser } from "../actions/UserAction";

import {
  Card,
  CardBody,
  CardFooter,
  CardTitle,
  Row,
  Col,
  Spinner,
} from "reactstrap";

// Datatable bootstrap 4
import "datatables.net-bs4/css/dataTables.bootstrap4.min.css";
import "datatables.net-bs4/js/dataTables.bootstrap4.min.js";

// Jquery
import "jquery/dist/jquery.min.js";
import $ from "jquery";

import { Link } from "react-router-dom";

import React, { Component } from "react";
import { connect } from "react-redux";

class Dashboard extends Component {
  componentDidMount() {
    // getListLiga
    this.props.dispatch(getListLiga());

    // getListJersey
    this.props.dispatch(getListJersey());

    // getListPesanan
    this.props.dispatch(getListPesanan());

    // getListUser
    this.props.dispatch(getListUser());

    // Initialize datatable
    $(document).ready(function () {
      setTimeout(function () {
        $("#myTable").DataTable({
          pageLength: 3,
          lengthMenu: [3, 10, 50, 100],
        });
      }, 1000);
    });
  }

  render() {
    // Props
    const {
      getListLigaResult,
      getListJerseyResult,
      getListPesananResult,

      getListUserResult,
      getListUserLoading,
      getListUserError,
    } = this.props;

    return (
      <div className="content">
        <Row>
          {/* Total Liga */}
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-world-2 text-warning" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Total Liga</p>
                      <CardTitle tag="p">
                        {getListLigaResult == null
                          ? "0"
                          : Object.keys(getListLigaResult).length}
                      </CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <div className="stats">
                  <Link to="/admin/liga">Go To Page</Link>
                </div>
              </CardFooter>
            </Card>
          </Col>
          {/* End Total Liga */}

          {/* Total Jersey */}
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-cart-simple text-danger" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Total Jersey</p>
                      <CardTitle tag="p">
                        {getListJerseyResult == null
                          ? "0"
                          : Object.keys(getListJerseyResult).length}
                      </CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <Link to="/admin/jersey">Go To Page</Link>
              </CardFooter>
            </Card>
          </Col>
          {/* End Total Jersey */}

          {/* Total Pesanan */}
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-money-coins text-success" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Total Pesanan</p>
                      <CardTitle tag="p">
                        {getListPesananResult == null
                          ? "0"
                          : Object.keys(getListPesananResult).length}
                      </CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <Link to="/admin/pesanan">Go To Page</Link>
              </CardFooter>
            </Card>
          </Col>
          {/* End Total Pesanan */}

          {/* Total User */}
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-single-02 text-primary" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Total User</p>
                      <CardTitle tag="p">
                        {Object.keys(getListUserResult).length}
                      </CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <div className="stats">Total User</div>
              </CardFooter>
            </Card>
          </Col>
          {/* End Total User */}
        </Row>

        <Row>
          <Col md="12">
            <Card>
              <CardBody>
                <Row className="align-items-center">
                  <Col md="12">
                    <table class="table table-hover" id="myTable">
                      <thead>
                        <tr>
                          <th scope="col">Nama</th>
                          <th scope="col">No Hp / Wa</th>
                          <th scope="col">Email</th>
                          <th scope="col">Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        {/* Jika datanya ada */}
                        {getListUserResult ? (
                          Object.keys(getListUserResult).map((key) => (
                            <tr key={key}>
                              <td>{getListUserResult[key].nama}</td>
                              <td>
                                <a className="btn btn-sm btn-success" href={`https://api.whatsapp.com/send?phone=62${getListUserResult[key].nohp.slice(1)}`}>
                                  <i class="fab fa-whatsapp"></i> {getListUserResult[key].nohp}
                                </a>
                              </td>
                              <td>{getListUserResult[key].email}</td>
                              <td>
                                {getListUserResult[key].status === "admin" ?
                                  <span className="badge badge-warning text-white">{getListUserResult[key].status} <i class="fas fa-check"></i></span> : <span className="badge badge-primary">{getListUserResult[key].status}</span>}
                              </td>
                            </tr>
                          ))
                        ) : // Jika loading
                          getListUserLoading ? (
                            <tr>
                              <td colSpan="3" align="center">
                                <Spinner color="primary" />
                              </td>
                            </tr>
                          ) : // Jika error
                            getListUserError ? (
                              <tr>
                                <td colSpan="3" align="center">
                                  {getListUserError}
                                </td>
                              </tr>
                            ) : (
                              // Jika data kosong
                              <tr>
                                <td colSpan="3" align="center">
                                  Data Kosong
                                </td>
                              </tr>
                            )}
                      </tbody>
                    </table>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
// mapStateToProps
const mapStateToProps = (state) => ({
  getListLigaResult: state.LigaReducer.getListLigaResult,
  getListJerseyResult: state.JerseyReducer.getListJerseyResult,
  getListPesananResult: state.PesananReducer.getListPesananResult,

  getListUserResult: state.UserReducer.getListUserResult,
  getListUserLoading: state.UserReducer.getListUserLoading,
  getListUserError: state.UserReducer.getListUserError,
});

export default connect(mapStateToProps, null)(Dashboard);
