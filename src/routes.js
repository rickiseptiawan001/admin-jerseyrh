import {
  Dashboard,

  ListLiga,
  TambahLiga,
  EditLiga,

  ListJersey,
  TambahJersey,
  EditJersey,

  ListPesanan,
  DetailPesanan,
} from "views";

var routes = [
  // Route dashboard
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Dashboard,
    layout: "/admin",
    sidebar: true,
  },

  // Route master liga
  {
    path: "/liga",
    name: "Master Liga",
    icon: "nc-icon nc-world-2",
    component: ListLiga,
    layout: "/admin",
    sidebar: true,
  },

  // Route tambah liga
  {
    path: "/liga/tambah",
    name: "Tambah Liga",
    component: TambahLiga,
    layout: "/admin",
    sidebar: false,
  },

  // Route edit liga
  {
    path: "/liga/edit/:id",
    name: "Edit Liga",
    component: EditLiga,
    layout: "/admin",
    sidebar: false,
  },

  // Route master jersey
  {
    path: "/jersey",
    name: "Master Jersey",
    icon: "nc-icon nc-cart-simple",
    component: ListJersey,
    layout: "/admin",
    sidebar: true,
  },

  // Route tambah jersey
  {
    path: "/jersey/tambah",
    name: "Tambah Jersey",
    component: TambahJersey,
    layout: "/admin",
    sidebar: false,
  },

  // Route edit jersey
  {
    path: "/jersey/edit/:id",
    name: "Edit Jersey",
    component: EditJersey,
    layout: "/admin",
    sidebar: false,
  },

  // Route master pesanan
  {
    path: "/pesanan",
    name: "Master Pesanan",
    icon: "nc-icon nc-single-copy-04",
    component: ListPesanan,
    layout: "/admin",
    sidebar: true,
  },

  // Route save laporan
  {
    path: "/pesanan/:id",
    name: "Detail Pesanan",
    component: DetailPesanan,
    layout: "/admin",
    sidebar: false,
  },
];
export default routes;
