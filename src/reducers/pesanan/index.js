// eslint-disable-next-line
import {
  GET_LIST_PESANAN,
  GET_DETAIL_PESANAN,
  UPDATE_PESANAN,
} from "../../actions/PesananAction";

const initialState = {
  getListPesananLoading: false,
  getListPesananResult: false,
  getListPesananError: false,

  getDetailPesananLoading: false,
  getDetailPesananResult: false,
  getDetailPesananError: false,

  updatePesananLoading: false,
  updatePesananResult: false,
  updatePesananError: false,
};

export default function foo(state = initialState, action) {
  switch (action.type) {
    case GET_LIST_PESANAN:
      return {
        ...state,
        getListPesananLoading: action.payload.loading,
        getListPesananResult: action.payload.data,
        getListPesananError: action.payload.errorMessage,
      };

    case UPDATE_PESANAN:
      return {
        ...state,
        updatePesananLoading: action.payload.loading,
        updatePesananResult: action.payload.data,
        updatePesananError: action.payload.errorMessage,
      };

    case GET_DETAIL_PESANAN:
      return {
        ...state,
        getDetailPesananLoading: action.payload.loading,
        getDetailPesananResult: action.payload.data,
        getDetailPesananError: action.payload.errorMessage,
      };
    default:
      return state;
  }
}
