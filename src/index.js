import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { Provider } from 'react-redux'
import store from './reducers/store'
import AdminLayout from "layouts/Admin.js"
import { Login, Finish, UnFinish, Gagal } from './views'

import "bootstrap/dist/css/bootstrap.css";
import "assets/scss/paper-dashboard.scss?v=1.3.0";
import "assets/demo/demo.css";
import "perfect-scrollbar/css/perfect-scrollbar.css";

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <Switch>
        <Route path="/admin" render={(props) => <AdminLayout {...props} />} />
        <Route path="/login" component={Login} exact />

        {/* Midtrans */}
        <Route path="/payment/finish" component={Finish} exact />
        <Route path="/payment/unfinish" component={UnFinish} exact />
        <Route path="/payment/error" component={Gagal} exact />


        <Redirect to="/login" />
      </Switch>
    </BrowserRouter>
  </Provider>,
  document.getElementById("root")
);

