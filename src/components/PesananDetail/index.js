import React from "react";
import { Row, Col, Card } from 'reactstrap';
import { rupiah } from '../../utils'

const PesananDetail = ({ pesanans }) => {
  return (
    <div>
      {Object.keys(pesanans).map((key) => {
        return (
          <Card style={{ padding: 10, marginTop: 10 }}>
            <Row key={key}>
              <Col md={12}>
                <Row className="justify-content-center">
                  <Col md={3}>
                    <img
                      src={pesanans[key].product.gambar[0]}
                      alt={pesanans[key].product.nama}
                    />
                  </Col>

                  <Col md={8}>
                    <p>{pesanans[key].product.nama}</p>
                    <p>Harga : Rp {rupiah(pesanans[key].product.harga)}</p>
                    <p>Jumlah Pesan : {pesanans[key].jumlahPesan}</p>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Card>
        )
      })}
    </div>
  );
};

export default PesananDetail;
