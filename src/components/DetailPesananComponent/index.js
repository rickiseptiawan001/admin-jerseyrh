import React from "react";
import {
  Table,
} from "reactstrap";
import { rupiah } from '../../utils'

const DetailPesananComponent = ({ pesanans }) => {
  return (
    <div>
      <div>
        <Table borderless className="text-left">
          <thead>
            <tr>
              <td><b>Produk</b></td>
              <td><b>Harga</b></td>
              <td><b>Jumlah Pesan</b></td>
            </tr>
          </thead>

          <tbody>
            {pesanans ? (
              Object.keys(pesanans).map((key) => {
                return (
                  <tr>
                    <td>{pesanans[key].product.nama}</td>
                    <td>Rp {rupiah(parseInt(pesanans[key].product.harga))}</td>
                    <td><span className="mr-3"><b>X</b></span> {pesanans[key].jumlahPesan}</td>
                  </tr>
                )
              })
            ) : (
              <p>Data Kosong</p>
            )}
          </tbody>
        </Table>
      </div>
    </div>
  );
};

export default DetailPesananComponent;
